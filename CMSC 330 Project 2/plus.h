//
// Created by yousuf Nejati on 12/9/15.
//

#ifndef CMSC_330_PROJECT_2_PLUS_H
#define CMSC_330_PROJECT_2_PLUS_H

#endif //CMSC_330_PROJECT_2_PLUS_H

class Plus : public SubExpression {
public:
    Plus(Expression *left, Expression *right) :
            SubExpression(left, right) {
    }

    int evaluate() {
        return left->evaluate() + right->evaluate();
    }
};