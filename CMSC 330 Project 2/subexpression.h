//
// Created by yousuf Nejati on 12/9/15.
//

#ifndef CMSC_330_PROJECT_2_SUBEXPRESSION_H
#define CMSC_330_PROJECT_2_SUBEXPRESSION_H



class SubExpression: public Expression
{
public:
    SubExpression(Expression* left, Expression* right);

    SubExpression(Expression* left, Expression* right, Expression* condition);

    SubExpression(Expression* left);

    static Expression* parse();

protected:
    Expression* left;
    Expression* right;
    Expression* condition;
};
#endif //CMSC_330_PROJECT_2_SUBEXPRESSION_H