//
// Created by yousuf Nejati on 12/9/15.
//

#ifndef CMSC_330_PROJECT_2_SYMBOLTABLE_H
#define CMSC_330_PROJECT_2_SYMBOLTABLE_H

#endif //CMSC_330_PROJECT_2_SYMBOLTABLE_H

class SymbolTable {
public:
    SymbolTable() { }

    void insert(string variable, double value);

    //void clearSymbolTable();

    double lookUp(string variable) const;

private:
    struct Symbol {
        Symbol(string variable, double value) {
            this->variable = variable;
            this->value = value;
        }

        string variable;
        double value;
    };

    static vector<Symbol> elements;

};