//
// Created by yousuf Nejati on 12/9/15.
//

#ifndef CMSC_330_PROJECT_2_TIMES_H
#define CMSC_330_PROJECT_2_TIMES_H

#endif //CMSC_330_PROJECT_2_TIMES_H

class Times: public SubExpression {

public:
    Times(Expression *left, Expression *right) : SubExpression(left, right) { }

    int evaluate()
    {
        return left->evaluate() * right->evaluate();
    }
};