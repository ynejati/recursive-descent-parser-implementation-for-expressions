//
// Created by yousuf Nejati on 12/9/15.
//

#ifndef CMSC_330_PROJECT_2_DIVIDE_H
#define CMSC_330_PROJECT_2_DIVIDE_H



class Divide : public SubExpression {

public:
    Divide(Expression *left, Expression *right) : SubExpression(left, right) { }


    int evaluate()
    {
        return left->evaluate() / right->evaluate();
    }
};

#endif //CMSC_330_PROJECT_2_DIVIDE_H