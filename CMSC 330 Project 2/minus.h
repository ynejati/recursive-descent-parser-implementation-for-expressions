//
// Created by yousuf Nejati on 12/9/15.
//

#ifndef CMSC_330_PROJECT_2_MINUS_H
#define CMSC_330_PROJECT_2_MINUS_H



class Minus : public SubExpression {


public:
    Minus(Expression *left, Expression *right) : SubExpression(left, right) { }

    int evaluate()
    {
        return left->evaluate() - right->evaluate();
    }
};

#endif //CMSC_330_PROJECT_2_MINUS_H
