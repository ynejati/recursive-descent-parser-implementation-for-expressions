//
// Created by yousuf Nejati on 12/9/15.
//

#include <strstream>
#include <vector>
using namespace std;

#include "expression.h"
#include "operand.h"
#include "variable.h"
#include "symboltable.h"

extern SymbolTable symbolTable;

int Variable::evaluate()
{
    return (int) symbolTable.lookUp(name);
}