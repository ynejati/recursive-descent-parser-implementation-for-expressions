//
// Created by yousuf Nejati on 12/9/15.
//

#ifndef CMSC_330_PROJECT_2_OPERAND_H
#define CMSC_330_PROJECT_2_OPERAND_H



class Operand: public Expression
{
public:
    static Expression* parse();
};
#endif //CMSC_330_PROJECT_2_OPERAND_H