//
// Created by yousuf Nejati on 12/9/15.
//

#ifndef CMSC_330_PROJECT_2_EXPRESSION_H
#define CMSC_330_PROJECT_2_EXPRESSION_H



class Expression
{
public:
    virtual int evaluate() = 0;
};

#endif //CMSC_330_PROJECT_2_EXPRESSION_H