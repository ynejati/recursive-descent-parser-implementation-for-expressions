//
// Created by yousuf Nejati on 12/10/15.
//

#ifndef CMSC_330_PROJECT_2_NEGATIONOPERATOR_H
#define CMSC_330_PROJECT_2_NEGATIONOPERATOR_H


class NegationOperator : public SubExpression {

public:
    NegationOperator(Expression *left) : SubExpression(left) {

    }

    int evaluate() {

        return !(left -> evaluate());
    }
};

#endif //CMSC_330_PROJECT_2_NEGATIONOPERATOR_H

