//
// Created by yousuf Nejati on 12/9/15.
//

#ifndef CMSC_330_PROJECT_2_VARIABLE_H
#define CMSC_330_PROJECT_2_VARIABLE_H

class Variable: public Operand
{
public:
    Variable(string name)
    {
        this->name = name;
    }
    int evaluate();

private:
    string name;
};

#endif //CMSC_330_PROJECT_2_VARIABLE_H

