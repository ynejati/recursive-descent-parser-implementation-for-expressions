//
// Created by yousuf Nejati on 12/9/15.
//

#include <iostream>

using namespace std;

#include "expression.h"
#include "subexpression.h"
#include "operand.h"
#include "plus.h"
#include "minus.h"
#include "times.h"
#include "divide.h"
#include "logicaland.h"
#include "logicalor.h"
#include "equals.h"
#include "ternaryOperator.h"
#include "negationOperator.h"
#include "greaterthan.h"
#include "lessthan.h"

SubExpression::SubExpression(Expression *left, Expression *right) {
    this->left = left;
    this->right = right;
}

SubExpression::SubExpression(Expression *trueValue, Expression *falseValue, Expression *conditional) {
    this->left = trueValue;
    this->right = falseValue;
    this->condition = conditional;
}

SubExpression::SubExpression(Expression *left) {
    this->left = left;
}


Expression *SubExpression::parse() {

    Expression *left;
    Expression *right;
    char operation, paren, questionMark, negation;
    left = Operand::parse();

    if (cin.peek() == '!') {
        cin >> negation;
        cin >> paren;
        return new NegationOperator(left);
    }

    cin >> operation; // takes in the operator
    right = Operand::parse();
    cin >> ws;

    if (cin.peek() == ')') {

        cin >> paren;
        switch (operation) {
            case '+':
                return new Plus(left, right);
            case '-':
                return new Minus(left, right);
            case '*':
                return new Times(left, right);
            case '/':
                return new Divide(left, right);
            case '>':
                return new GreaterThan(left,right);
            case '<':
                return new LessThan(left,right);
            case '&':
                return new LogicalAnd(left, right);
            case '|':
                return new LogicalOr(left, right);
            case '=':
                return new LogicalEquals(left, right);
            default:break;
        }

    } if (cin.peek() == '?' && operation == ':') {
        cin >> questionMark;
        Expression *trueValue = left;
        Expression *falseValue = right;
        Expression *conditional = SubExpression::parse();
        return new TernaryOperator(trueValue, falseValue, conditional);
    }
    cin >> paren;
    return 0;
}