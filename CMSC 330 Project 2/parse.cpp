//
// Created by yousuf Nejati on 12/9/15.
//

#include <cctype>
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

#include "parse.h"


// Creates a string representing the left side of the nonalphanumeric
string parseName()
{
    char alnum;
    string name = "";

    cin >> ws; // all whitespace should be ignored
    while (isalnum(cin.peek())) // checks if the char is a decimal digit or a upper or lowercase letter
        // returns the next input character without taking it our of the input stream
    {
        // while the next is alphanumeric
        cin >> alnum;
        // add the alphanumeric to the string
        name += alnum;
    }
    // return the string
    return name;

}