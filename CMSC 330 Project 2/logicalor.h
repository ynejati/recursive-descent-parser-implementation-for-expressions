//
// Created by yousuf Nejati on 12/9/15.
//

#ifndef CMSC_330_PROJECT_2_LOGICALOR_H
#define CMSC_330_PROJECT_2_LOGICALOR_H

#include "subexpression.h"
#include "expression.h"

class LogicalOr : public SubExpression {

public:
    LogicalOr(Expression *left, Expression *right) : SubExpression(left, right) { }

    int evaluate()
    {
        return left->evaluate() || right->evaluate();
    }
};



#endif //CMSC_330_PROJECT_2_LOGICALOR_H
