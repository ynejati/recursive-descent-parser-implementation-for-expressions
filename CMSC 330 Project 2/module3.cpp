#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include "expression.h"
#include "subexpression.h"
#include "symboltable.h"
#include "parse.h"
#include <fstream>
#include <sstream>

SymbolTable symbolTable;

void parseAssignments();

int main() {

    ifstream filestream("/Users/yousufnejati/ClionProjects/CMSC 330 Project 2/project2TestInput");
    streambuf *cinbuf = cin.rdbuf();
    cin.rdbuf(filestream.rdbuf());
    string line;

    if (filestream.is_open()) {

        while (getline(filestream, line)) {

          //cout << line << endl;

            Expression *expression;
            char paren, comma;
            cin >> paren;
            expression = SubExpression::parse();
            if (cin.peek() == ',') {
                cin >> comma;
                parseAssignments();
            }
            cout << "Value = " << expression->evaluate() << endl;
           // symbolTable.clearSymbolTable();
        }
    } else cout << "Unable to read file";

    cin.rdbuf(cinbuf);
    filestream.close();
}


void parseAssignments() {
    char assignop, delimiter;
    string variable;
    double value;
    do {
        variable = parseName();

        cin >> ws >> assignop >> value >> delimiter;

        symbolTable.insert(variable, value);
    }
    while (delimiter == ',');
}




