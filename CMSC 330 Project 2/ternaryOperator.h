//
// Created by yousuf Nejati on 12/10/15.
//

#ifndef CMSC_330_PROJECT_2_TERNARYOPERATOR_H
#define CMSC_330_PROJECT_2_TERNARYOPERATOR_H
#endif //CMSC_330_PROJECT_2_TERNARYOPERATOR_H

#include "expression.h"
#include "subexpression.h"
#include "operand.h"
#include <iostream>

class TernaryOperator : public SubExpression {


public:
    TernaryOperator(Expression *trueCase, Expression *falseCase, Expression *condition) : SubExpression(
            trueCase, falseCase, condition) {

        this->left = trueCase;
        this->right = falseCase;
        this->condition = condition;
    }


    int evaluate() {

        return condition->evaluate() ? left->evaluate():right->evaluate();
    }

};

