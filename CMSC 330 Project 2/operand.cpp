//
// Created by yousuf Nejati on 12/9/15.
//

#include <cctype>
#include <iostream>
#include <list>
#include <string>
#include <sstream>

using namespace std;

#include "expression.h"
#include "subexpression.h"
#include "operand.h"
#include "variable.h"
#include "literal.h"
#include "parse.h"

Expression *Operand::parse() {
    char paren;
    double value;

   cin >> ws;
    if (isdigit(cin.peek())) // Operand can be a literal
    {
       cin >> value;
        Expression *literal = new Literal((int) value);
        return literal;
    }
    if (cin.peek() == '(') {
       cin >> paren;
        return SubExpression::parse(); // can be an expression
    }
    else
        return new Variable(parseName()); // can be a variable
}