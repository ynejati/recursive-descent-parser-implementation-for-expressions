//
// Created by yousuf Nejati on 12/9/15.
//

#include "operand.h"

#ifndef CMSC_330_PROJECT_2_LITERAL_H
#define CMSC_330_PROJECT_2_LITERAL_H



class Literal: public Operand
{
public:
    Literal(int value)
    {
        this->value = value;
    }
    int evaluate()
    {
        return value;
    }
private:
    int value;
};

#endif //CMSC_330_PROJECT_2_LITERAL_H